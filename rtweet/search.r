# Title: Searching Tweets
# State: Worked

library("rtweet")
setwd("D:/dbt/01/git/lab/r-lab/rtweet")

rt <- search_tweets(
  "#rstats", n = 1000, include_rts = FALSE
)