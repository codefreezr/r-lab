# Title: Plots Twitter Stats
# State: Experiment

library("rtweet")

## search for tweets containing "rstats"
rt <- search_tweets("rstats", n = 10000)

## plot frequency in 1 min intervals
ts_plot(rt, "mins")

## plot multiple time series--retweets vs non-retweets
rt %>%
  dplyr::group_by(is_retweet) %>%
  ts_plot("hours")

## compare account activity for some important US political figures
tmls <- get_timeline(
  c("SenSchumer", "SenGillibrand", "realDonaldTrump"),
  n = 3000
)

## examine all twitter activity using weekly intervals
ts_plot(tmls, "weeks")

## group by screen name and plot each time series
ts_plot(dplyr::group_by(tmls, screen_name), "weeks")

## group by screen name and is_retweet
tmls %>%
  dplyr::group_by(tmls, screen_name, is_retweet) %>%
  ts_plot("months")