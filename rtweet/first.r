# Title: Experiments with rtweet Package
# State: Collection

library("rtweet")

myLists <- lists_users("DetlefBurkhardt", reverse=TRUE)

tmls <- get_timelines("DetlefBurkhardt", n = 1000)

get_my_timeline()


hrc <- search_tweets(q = "hillaryclinton", n = 1000)

## data frame where each observation (row) is a different tweet
hrc


## get most recent 3200 tweets posted by Donald Trump's account
djt <- get_timeline("realDonaldTrump", n = 3200)

## data frame where each observation (row) is a different tweet
djt

#myCol <- get_collections("DetlefBurkhardt")

# browseURL(sprintf("https://twitter.com/%s/lists/DevNews",
#                   rtweet:::home_user()))
# 
# 
# browseURL(sprintf("https://twitter.com/%s/moments",
#                   rtweet:::home_user()))
# 
# browseURL(sprintf("https://twitter.com/%s/timelines/1332496424571965442",
#                   rtweet:::home_user()))



# tweet_shot("947082036019388416")
# tweet_shot("https://twitter.com/jhollist/status/947082036019388416")
# 
# 
# 
# trends <- trends_available()
# trends
# Essen <- get_trends(woeid = "Essen")



# setwd("D:/dbt/01/git/lab/r-lab/rtweet")
# 
# rt <- search_tweets(
#   "#rc3", n = 1000, lang = "de", include_rts = FALSE
# )


#cnn_fds <- get_friends("DetlefBurkhardt")
#cnn_fds_data <- lookup_users(cnn_fds$user_id)

#cnn_flw <- get_followers("DetlefBurkhardt")
#cnn_flw_data <- lookup_users(cnn_flw$user_id)

#tmls <- get_timelines("DetlefBurkhardt", n = 1000)

#sf <- get_trends("Germany")

#names(tmls)
