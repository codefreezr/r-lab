# Title: Using more transparency
# State: Worked

library(magick)
iFill <- function (myImage, myColor) {
  keith_filled <- image_fill(myImage, myColor, "+10,+10", fuzz=55,refcolor = NULL)
  return(keith_filled)
}

setwd("D:/dbt/01/git/lab/r-lab")
keith <- image_read('keith.svg')


keith.gray <- iFill(keith,"gray")
print(keith.gray)

keith_trans <- image_transparent(keith.gray,"gray",fuzz=1)
print(keith_trans)


image_write(keith_trans,'myKeith-gray.svg','svg')