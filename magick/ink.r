# Title: Using Composite
# State: Worked

library(magick)
setwd("D:/dbt/01/git/lab/r-lab/magick")

inks <- image_read('img/768px-Inkscape_logo.png')
tapete <- image_read('img/Fototapete.jpg')

combine <- image_composite(tapete, inks)
print(combine)