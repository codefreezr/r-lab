# Title: Using transparency
# State: Worked

library(magick)
iFill <- function (myImage, myColor) {
  keith_filled <- image_fill(myImage, myColor, point = "+197,+253", fuzz=55,refcolor = NULL)
  return(keith_filled)
}



setwd("D:/dbt/01/git/lab/r-lab")
keith <- image_read('keith-three.jpg')
keith.yellow <- iFill(keith,"yellow")

keith_trans <- image_transparent(keith.yellow,"yellow",fuzz=56)
print(keith_trans)


image_write(keith_trans,'myKeith-yellow.png','png')