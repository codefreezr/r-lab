# Title: Complete Mini Pop-Art Project
# State: Worked

library(magick)
setwd("D:/dbt/01/git/lab/r-lab")
keith <- image_read('keith-dance.jpg')

iFill <- function (myImage, myColor) {
  keith_filled <- image_fill(myImage, myColor, point = "+10,+10", fuzz=55,refcolor = NULL)
  return(keith_filled)
}

keith.blue <- iFill(keith,"blue")
keith.yellow <- iFill(image_flop(keith),"yellow")
keith.top <- image_append(c(keith.blue,keith.yellow))

keith.green <- iFill(image_flip(keith),"green")
keith.red <- iFill(image_flip(keith.yellow),"red")
keith.bottom <- image_append(c(keith.green,keith.red))

keith.all <- image_append(c(keith.top,keith.bottom), stack = TRUE)

print(keith.all)
