# Title: Analysing hashtags of Gradle-Tweets
# State: Worked

library(rvest)
library(dplyr)
setwd("D:/dbt/01/git/lab/r-lab/rvest")

baseURL <- "https://plugins.gradle.org/search?page="

taglist <- character()
for(n in 0:484) {
  print(n)
  loadURL <- paste(baseURL,n,sep="")
  page <- read_html(loadURL)
  tags <- page %>%
    html_nodes(".plugin-tag") %>%
    html_text(trim=TRUE) %>%
    substring(2)
    taglist <- c(taglist,tags)
}
length(unique(taglist))

output <- matrix(unlist(taglist), ncol = 1, byrow = TRUE)

fileConn<-file("output.txt")
writeLines(taglist, fileConn)
close(fileConn)

#4836
#ctag <- c(taglist)
#ctag2 <- group_by(output)
                 
#data %>% group_by(as.vector(taglist)) %>% summarize(count=n())