# Title: First Reading of an Excel-Sheet
# State: Started

library("googlesheets4")
library("readxl")
library("sqldf")
setwd("D:/dbt/01/git/lab/r-lab/sql")

djsets <- read_xlsx("djsets.xlsx")

mysets <- sqldf("select title 
                 from djsets limit 3,6")
mysets