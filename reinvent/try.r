# Title: Analysing HTML-Content
# State: Started

library(rvest)
library(magick)

setwd("D:/dbt/01/git/lab/r-lab/reinvent")
mySession <- read_html("AdvertisingMarketing-1_0n1tx6s5.html")


detail 

rating <- lego_movie %>% 
  html_nodes("strong span") %>%
  html_text() %>%
  as.numeric()
rating

cast <- lego_movie %>%
  html_nodes("#titleCast .primary_photo img") %>%
  html_attr("alt")
cast

poster <- lego_movie %>%
  html_nodes(".poster img") %>%
  html_attr("src")

p <- image_read(poster)
print(p)

