# Title: Complete Mass-Imagemanipulation for Track-Overview
# State: Worked

library(magick); library(textclean)
#textFill("reInvent_Sessions_ManagementGovernence_MGT.jpg")
setwd("D:/dbt/01/git/lab/r-lab/reinvent")


textFill <- function (myImageName){
  
  
  source = paste("img/from/",{myImageName}, sep = "")
  target = paste("img/to/",{myImageName}, sep = "")
  
  v <- unlist(strsplit(source, "_"))
  trackTitle <- strip(v[3], lower.case = FALSE)
  trackShort <- substr(v[4],1,3)
    
  
  trackImage <- image_read({source})
  
  t.black <- image_annotate(trackImage, {trackShort}, font = "mono", weight="600", size = 40, color = "black",  degrees = -90, gravity="Center", location="-234+1")
  t.left <- image_annotate(t.black, {trackShort}, font = "mono", weight="600", size = 40, color = "white",  degrees = -90, gravity="Center", location="-236-1")
  
  fontname = "Impact"
  t.med <- image_annotate(t.left, {trackTitle}, font = {fontname}, weight="200", size = 36, color = "black", gravity="Center", location="+70-0")
  t.all <- image_annotate(t.med, {trackTitle}, font = {fontname}, weight="200", size = 36, color = "white", gravity="Center", location="+68-2")
  t.all  
  image_write(t.all,{target},'jpg')
}


filenames <- basename(Sys.glob("img/from/*.jpg"))
n = 0
for (i in filenames) {
  textFill(i)
}







