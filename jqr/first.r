# Title: First Experiments with jqr.
# State: Worked

library(jqr)

str <- '[{
    "foo": 1,
    "bar": 2
  },
  {
    "foo": 3,
    "bar": 4
  },
  {
    "foo": 5,
    "bar": 6
}]'

jq(str, ".[]")

str <- '["a","b,c,d","e"]'
jq(str, 'join(", ")')


