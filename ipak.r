# Title: Installs a List of Packages via Artifactory
# State: Worked

ipak <- function(pkg){
    new.pkg <- pkg[!(pkg %in% installed.packages()[, "Package"])]
    if (length(new.pkg)) 
        install.packages(new.pkg, dependencies = TRUE, repos="https://bahnhub.tech.rz.db.de/artifactory/cran-remote/")
    sapply(pkg, require, character.only = TRUE)
}

# usage
packages <- c("ggimage", "magick", "RDCOMClient", "rsvg")
ipak(packages)